<?php

/*
 * Spanish language
 */

$lang['text_rest_invalid_api_key'] = 'API key no válida %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Credenciales no válidas';
$lang['text_rest_ip_denied'] = 'IP denegada';
$lang['text_rest_ip_unauthorized'] = 'IP no autorizada';
$lang['text_rest_unauthorized'] = 'No autorizado';
$lang['text_rest_ajax_only'] = 'Solamente se permiten peticiones AJAX';
$lang['text_rest_api_key_unauthorized'] = 'Esta API key no tiene acceso a al controller solicitado';
$lang['text_rest_api_key_permissions'] = 'Esta API key no tiene suficientes permisos';
$lang['text_rest_api_key_time_limit'] = 'Esta API key ha alcanzado el límite de uso para este método';
$lang['text_rest_unknown_method'] = 'Método desconocido';
$lang['text_rest_unsupported'] = 'Protocolo no soportado';
